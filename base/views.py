from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required


@login_required
def index(request):
    return render(request, 'base/index.html', {})


@login_required
def detail_1(request):
    return render(request, 'base/detail_1.html', {})


@login_required
def detail_2(request):
    return render(request, 'base/detail_2.html', {})


@login_required
def detail_3(request):
    return render(request, 'base/detail_3.html', {})


@login_required
def detail_4(request):
    return render(request, 'base/detail_4.html', {})


@login_required
def detail_5(request):
    return render(request, 'base/detail_5.html', {})
