from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'company/$', CompanyListView.as_view(), name="company-list"),
    url(r'company/create/$', CompanyCreateView.as_view(), name="company-create"),
    url(r'company/update/(?P<pk>[0-9]+)/$', CompanyUpdateView.as_view(), name="company-update"),
    url(r'company/delete/(?P<pk>[0-9]+)/$', CompanyDeleteView.as_view(), name="company-delete"),
]
