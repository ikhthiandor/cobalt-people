from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, ButtonHolder
from crispy_forms.bootstrap import FormActions

from .models import Company



class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        exclude = ['created_by', 'updated_by', 'version', 'code', 'is_locked']

    def __init__(self, *args, **kwargs):
        super(CompanyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.attrs = {'novalidate': ''}
        self.helper.form_id = 'id-company-form'
        self.helper.form_class = 'form'
        self.helper.form_method = 'post'
        self.helper.form_action = '.'

        self.helper.layout = Layout(
            Fieldset(
                'General Information',
                'name',
                'license',
                'summary',
                'address',
                'established_at',
                'website',
                'phone_numbers',
                'mobile_numbers',
                'emails',
                'fax',
                'remarks',
            ),
            ButtonHolder(
                Submit('submit', 'Save', css_class='btn btn-lg btn-primary')
            )
        )
