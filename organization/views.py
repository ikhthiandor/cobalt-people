from django.shortcuts import render
from django.views.generic import (ListView, CreateView, UpdateView, DeleteView, DetailView)

from .models import *
from .forms import *


class CompanyListView(ListView):
    model = Company

    def get_context_data(self, *args,**kwargs):
        ctx = super(CompanyListView, self).get_context_data(*args, **kwargs)
        ctx["page_title"] = "Company List"
        return ctx


class CompanyCreateView(CreateView):
    model = Company
    form_class = CompanyForm
    success_url = '/organization/company/'

    def get_context_data(self, *args,**kwargs):
        ctx = super(CompanyCreateView, self).get_context_data(*args, **kwargs)
        ctx["page_title"] = "Company Create"
        return ctx


class CompanyUpdateView(UpdateView):
    model = Company
    form_class = CompanyForm
    success_url = '/organization/company/'

    def get_context_data(self, *args, **kwargs):
        ctx = super(CompanyUpdateView, self).get_context_data(*args, **kwargs)
        ctx["page_title"] = "Company Update"
        return ctx


class CompanyDeleteView(DeleteView):
    model = Company
    success_url = '/organization/company/'

    def get_context_data(self, *args, **kwargs):
        ctx = super(CompanyDeleteView, self).get_context_data(*args, **kwargs)
        ctx["page_title"] = "Company Delete"
        return ctx