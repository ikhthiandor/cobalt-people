import datetime

from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField

from base.models import BaseModel, CodedBase


class Company(CodedBase):
    _prefix = "CODE"
    name = models.CharField(verbose_name=_('Name'), max_length=255)
    license = models.CharField(verbose_name=_('License'), unique=True, max_length=255)
    established_at = models.DateField(verbose_name=_('Established at'), blank=True, null=True)
    website = models.CharField(verbose_name=_('Website'), max_length=255)
    summary = models.TextField(verbose_name=_('Summary'),)
    address = models.TextField(verbose_name=_('Address'),)
    phone_numbers = models.CharField(verbose_name=_('Phone numbers'), max_length=255)
    mobile_numbers = models.CharField(verbose_name=_('Mobile numbers'), blank=True, null=True, max_length=255)
    fax = models.CharField(verbose_name=_('Fax'), max_length=255, blank=True, null=True)
    emails = models.CharField(verbose_name=_('Emails'), max_length=255)
    remarks = models.TextField(verbose_name=_('Remarks'), blank=True, null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = _("Companies")
        permissions = (
            ('view_company', "Can view company"),)


class Division(CodedBase):
    _prefix = "CODE"
    name = models.CharField(verbose_name=_('Name'), max_length=255)
    company = models.ForeignKey("organization.Company", blank=True, null=True)
    summary = models.TextField(verbose_name=_('Summary'), blank=True, null=True)
    remarks = models.TextField(verbose_name=_('Remarks'), blank=True, null=True)

    class Meta:
        ordering = ['name']
        permissions = (
            ('view_division', "Can view division"),)


class Department(CodedBase):
    _prefix = "CODE"
    name = models.CharField(verbose_name=_('Name'),  max_length=255)
    department_code = models.CharField(verbose_name=_("Department Code"), max_length=255, blank=True, null=True)
    division = models.ForeignKey("organization.Division", blank=True, null=True)
    summary = models.TextField(verbose_name=_('Summary'),  blank=True, null=True)
    remarks = models.TextField(verbose_name=_('Remarks'),  blank=True, null=True)

    @property
    def is_department(self):
        return len(self.department_code) >= 5


    class Meta:
        ordering = ['name']
        permissions = (
            ('view_department', "Can view department"),)


class Role(CodedBase):
    _prefix = "ROL"
    title = models.CharField(verbose_name=_('Title'),  max_length=255)
    description = models.TextField(verbose_name=_('Description'), )
    level = models.IntegerField(verbose_name=_('Level'), )
    remarks = models.TextField(verbose_name=_('Remarks'),  blank=True, null=True)

    class Meta:
        ordering = ['title']
        permissions = (
            ('view_role', "Can view role"),)


class Designation(CodedBase):
    _prefix = "DSG"
    title = models.CharField(verbose_name=_('Title'), max_length=255)
    designation_code = models.CharField(verbose_name=_('Designation Code'), max_length=255, blank=True, null=True)
    department = models.ForeignKey("organization.Department", blank=True, null=True)
    reports_to = models.ForeignKey("organization.Designation",
                                   related_name="managed_set",
                                   verbose_name=_('Reports to'),
                                   blank=True, null=True)
    description = models.TextField(verbose_name=_('Description'), blank=True, null=True)
    level = models.IntegerField(verbose_name=_('Level'), )
    remarks = models.TextField(verbose_name=_('Remarks'),  blank=True, null=True)
    roles = models.ManyToManyField("organization.Role", blank=True, null=True)


    class Meta:
        ordering = ['title']
        permissions = (
            ('view_designation', "Can view designation"),)