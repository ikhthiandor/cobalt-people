from django.conf.urls import url, include
from django.contrib import admin

from django.contrib.auth.views import login, logout_then_login
from base.views import index, detail_1, detail_2, detail_3, detail_4, detail_5

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', login, {'template_name': 'base/login.html'}),
    url(r'^logout/$', logout_then_login, name="logout"),
    url(r'^organization/', include('organization.urls')),
    ## LEFT OVER
    url(r'^detail/1/', detail_1),
    url(r'^detail/2/', detail_2),
    url(r'^detail/3/', detail_3),
    url(r'^detail/4/', detail_4),
    url(r'^detail/5/', detail_5),
    ##
    url(r'^$', index)
]
